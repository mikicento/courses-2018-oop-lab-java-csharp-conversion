using System;

namespace Unibo.Oop.Mnk
{
    public interface IMNKEventListener
    {
        void OnTurnEnded(TurnEventArgs args);
        void OnTurnBeginning(TurnEventArgs args);
        void OnMatchEnded(MatchEventArgs args);
        void OnResetPerformed(IMNKMatch model);
        void OnErrorOccurred(Exception error);
    }
}