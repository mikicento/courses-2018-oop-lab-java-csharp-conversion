using System;
using Unibo.Oop.Utils;

namespace Unibo.Oop.Mnk
{
    public interface IMNKMatch
    {
        IImmutableMatrix<Symbols> Grid { get; }
        
        int Turn { get; }
        
        int K { get; }

        void Move(int i, int j);

        void Reset();
        
        Symbols CurrentPlayer { get; }

        event Action<TurnEventArgs> TurnBeginning;
        event Action<TurnEventArgs> TurnEnded;
        event Action<MatchEventArgs> MatchEnded;
        event Action<Exception> ErrorOccurred;
        event Action<IMNKMatch> ResetPerformed;
    }
}